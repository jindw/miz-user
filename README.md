## 米庄理财--用户自助提交审核凭证入口

### 项目背景

##### 用户自助提交审核凭证入口。

### 技术方案

* 规范：
	* ES6
	* CommonJS
* 基础库：
	* Reactjs
	* Reflux
	* ReactRouterComponent
	* Zepto
	* Q
* 样式：
	* Stylus
* 调试及构建：
	* JSX
	* Sourcemap
	* browserSync
	* Webpack
	* Gulp

### 测试demo

* [demo](index.html)
 
