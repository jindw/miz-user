var Signals = require('signals');
var Arrow_Right = require('../SvgIcon/Svg_Arrow_Right');
var Util = require('../../app/util');
var _assign = require('lodash/assign');

var Header = React.createClass({

    getInitialState: function() {
        return {
            title: '',
            backBtn: true,
            rightBtn: ''
        };
    },

    setHeader: function(cfg){
        var defaultSettings = {
            title: '',
            backBtn: true,
            rightBtn: ''
        };
        this.setState(_assign(defaultSettings, cfg));
    },

    back: function(){
        if(this.state.backBtn === 'home'){
            location.hash='';
            return;
        }
        if(window.history.length === 1){
            location.hash = '#';
            return;
        }else if(location.hash.indexOf('#/login') === 0){
            location.href=location.origin+'/feature/xplan';
        }
        window.history.back();
    },

    reloadPage: function(){
        window.location.reload();
    },

    componentDidMount : function(){
        var t = this;
        if(!window.__event__) window.__event__= {};
        __event__.setHeader = new Signals.Signal();
        __event__.setHeader.add(t.setHeader);

    },

    render: function() {
        var t = this;
        var title = this.state.title;
        var back_dom = this.state.backBtn ? (<div className="flex-h jc-start ai-center arrow-back tTap" onClick={this.back}>
                        <Arrow_Right size={18} fill="#c7c7cc"/>
                        <div></div>
                    </div>) : '';

        var right_btn;

        if(t.state.rightBtn){
            if(t.state.rightBtn === 'tel')
                right_btn = <a ref='tel' href="tel:4006998883" className='telephone'></a>
            else if(t.state.rightBtn ==='imageok'){
                right_btn = <a href="javascript:;" className='imageok'>确定</a>
            }else if(t.state.rightBtn === 'caips'){
                right_btn = <a href="#/caips" className='findprogress'>进度查询</a>
            }else if(t.state.rightBtn === 'bank-reason'){
                right_btn = <a href={location.origin+'/feature/exchangecard'} className='findprogress'>换卡说明</a>
            }
        }

        return (
            <div className="flex-h jc-center ai-center header border-bottom-1px">
                <div className="flex1">
                    {back_dom}
                </div>
                <div id="js_header_title" className="flex3 title">{title}</div>
                <div className="flex1">
                    <div className="flex-h jc-start ai-center">
                        {right_btn}
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Header;