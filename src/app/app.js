window.__event__ = {};

var Router = ReactRouter.Router;
var Route = ReactRouter.route;


//用户信息更换
var UserMsgChange               = require('../pages/UserMsgChange');

//身份证变更及升位
var ChangeIdCard                = require('../pages/ChangeIdCard');
//银行卡变更
var ChangeBankCard              = require('../pages/ChangeBankCard');
//手机号码变更
var ChangeMobile                = require('../pages/ChangeMobile');
//进度查询
var Caips                       = require('../pages/Caips');

// 页面头
var Header                      = require('../components/Header');

$(function() {

    var App = React.createClass({
        componentWillMount(){
            window.addEventListener('hashchange',function(){
                document.getElementsByClassName('body')[0].scrollTop = 0;
            })
        },

        render() {
            var t = this;
            return (
                <div className="us-container" style={{backgroundColor:'#FFF'}}>
                    <Header />
                    <div className="body" style={{minHeight:($(window).height())}}>
                        {this.props.children}
                    </div>
                </div>
            )
        }
    });

    var routes = {
        path: '/',
        component: App,
        indexRoute: { component: UserMsgChange },
        childRoutes: [
            { path: 'home',             component: UserMsgChange },
            { path: 'changeidcard',     component: ChangeIdCard },
            { path: 'changebankcard',   component: ChangeBankCard },
            { path: 'changemobile',     component: ChangeMobile },
            { path: 'caips',            component: Caips }

        ]
    };

    ReactDOM.render(<Router history={ReactRouter.hashHistory} routes={routes}/>, document.body);
});

