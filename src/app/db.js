var DBF = require('./dbFactory');
var Util = require('./util');
// 设置全局的`url`前缀
// 开发环境
if (__LOCAL__) {
    window.__loginUrl__ = '//mizlicai.chinacloudsites.cn/#/login?backTo=';
    var urlPrefixForMicroFlow = '//121.43.148.191:8108';
}

console.log(__LOCAL__)

// 生产环境
if (__PRO__) {
    var urlPrefixForMicroFlow = "//api.mizlicai.com";
    window.__loginUrl__ = "//h5.mizlicai.com/#/login?backTo=";
}
DBF.set('urlPrefix', urlPrefixForMicroFlow);

DBF.set('defaultParsePesp', function(resp){
    if(resp.status && resp.status.toLocaleLowerCase() === 'success'){
        return resp.data || {};
    }
    return '';
});

DBF.create('Progress', {
	getList : {
        url  	  : '/proof/progress.json',
        type      : 'GET'
    }
});
DBF.create('Program', {
    getMessage : {
        url       : '/proof/progress/{id}.json',
        type      : 'GET'
    }
});

DBF.create('Mobile', {
    addMobile : {
        url       : '/proof/upload/MOBILE.json',
        type      : 'POST',
        processData: false
    },
    changeMobile : {
        url       : '/proof/modify/MOBILE/{id}.json',
        type      : 'POST',
        processData: false
    },
    getUserInfo : {
        url       : '/proof/userInfo.json',
        type      : 'GET'
    },
    sendVerificationCode:{
        url       : '/register/sendVerificationCode_v2.json',
        type      : 'POST',
    },
    validate:{
        url       : '/proof/validate.json',
        type      : 'POST',
    },
    replaceMobile:{
        url       : '/proof/replaceMobile.json',
        type      : 'POST',
    },
    checkRegisterable:{
        url       : '/register/checkRegisterable.json',
        type      : 'POST',
    },
});

DBF.create('IDCard', {
    addIDCard : {
        url       : '/proof/upload/IDCARD.json',
        type      : 'POST',
        processData: false
    },
    changeIDCard : {
        url       : '/proof/modify/IDCARD/{id}.json',
        type      : 'POST',
        processData: false
    },
    idCardInfo:{
        url       : '/proof/idCardInfo.json',
        type      : 'get',
    },
});

DBF.create('BankCard', {
    addBankCard : {
        url       : '/proof/upload/BANKCARD.json',
        type      : 'POST',
        processData: false
    },
    changeBankCard : {
        url       : '/proof/modify/BANKCARD/{id}.json',
        type      : 'POST',
        processData: false
    },
    bankInfo: {
        url       : '/proof/bankInfo.json',
        type      : 'GET'
    },
});

module.exports = DBF.context;
