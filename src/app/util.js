module.exports = {
    getUrlParam: function(key) {
        var search = location.search;
        var arr = !search ? [] : location.search.substr(1).split('&');
        var param = {};
        for (var i=0, l=arr.length; i<l; i++) {
            var kv = arr[i].split('=');
            param[kv[0]] = kv[1];
        }
        return key ? (param[key] || '') : param;
    },
    getUserKey: function(){
        var userKey = localStorage.getItem('auth');
        if(!userKey){
            location.href = window.__loginUrl__+encodeURIComponent(location.href);
        }
    },
    setLocal: function(key, val){
        localStorage.setItem(key, JSON.stringify(val));
    },
    getLocal: function(key){
        if(localStorage.getItem(key)){
            return JSON.parse(localStorage.getItem(key));
        }else{
            return '';
        }
    },
    removeLocal: function(key){
        localStorage.removeItem(key);
    },
    //uuid生成
    getUnid: function(){
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    },
    upload(up,title,file,readonly,mtitle){
        var titles = mtitle;
        var t = this;
        if(file.parentNode.className.indexOf('ready')!==-1&&!document.getElementsByClassName('water').length){
            var a = document.createElement('a');
            a.style.backgroundSize='30%';
            a.className = 'showimg';
            if(file.files.length){
                var reader = new FileReader();
                reader.readAsDataURL(file.files[0]);
                reader.onload = function(e) {
                    var data = e.target.result;
                    var img = new Image;
                    img.src = data;
                    img.onload = function(){
                        if(img.width>document.body.scrollWidth){
                            a.style.backgroundSize='contain';
                        }else{
                            a.style.backgroundSize = '';
                        }
                        a.style.backgroundImage = 'url('+data+')';
                    }
                }
            }else{
                var id = file.parentNode.getAttribute('data-id');
                var img = new Image();
                var hos = 'http://121.43.148.191:8113';
                if(location.host === "h5.mizlicai.com"){
                    hos = 'http://api.mizlicai.com';
                }
                img.src = hos+'/proof/picView/'+id+'?os=h5&userKey='+localStorage.auth+'&time='+((new Date()).getTime());
                img.onload = function(){
                    a.style.backgroundImage = 'url('+img.src+')';
                    if(img.width>document.body.scrollWidth){
                        a.style.backgroundSize='contain';
                    }else{
                        a.style.backgroundSize = '';
                    }
                }
                img.onerror = function(){
                    $.get(img.src, function(data) {
                        var div = document.createElement('div');
                        div.className = 'errmsg';
                        div.appendChild(document.createTextNode(data.errorMsg||'请求失败，请稍后再试！'));
                        document.body.appendChild(div);
                        setTimeout(function(){
                            div.parentNode.removeChild(div);
                        },3000);
                        imgout();
                    });
                }

            }

            document.body.appendChild(a);//图片

            __event__.setHeader.dispatch({
                title: title,
                rightBtn: 'imageok',
                backBtn: false
            });

            var water = document.createElement('a');
            water.className = 'water';
            document.body.appendChild(water);//水印

            $('body').on('click', '.imageok', imgout);
            window.addEventListener('hashchange',function(){
                $('.water,.showimg,.operate').remove();
                $('.change').removeClass('change');
            });

            function imgout(){
                $('.water,.showimg').animate({
                    top: '100%'
                },300,'linear',function() {
                    $('.water,.showimg,.operate').remove();
                    $('.change').removeClass('change');
                    titles = document.getElementById('J_Title').value||titles;
                    __event__.setHeader.dispatch({
                        title: titles,
                        rightBtn: titles === '更换银行卡'?'bank-reason':false
                    });
                });
            }

            var operate = document.createElement('div');
            operate.className = 'operate';

            var ok = document.createElement('span');
            ok.addEventListener('click',imgout)
            // ok.className = 'imageok'
            var okword = document.createTextNode('确定');
            ok.appendChild(okword);

            if(readonly){
                operate.appendChild(ok);
                document.body.appendChild(operate);
                return;
            }
            // a.style.height = 'calc(100% - 44px)';
            

            var deletes = document.createElement('span');
            var deleteword = document.createTextNode('删除');
            deletes.appendChild(deleteword);

            var change = document.createElement('span');
            var changeword = document.createTextNode('更换');
            change.appendChild(changeword);
            var u = navigator.userAgent
            change.onclick = function(){
                file.click();
            }

            
            // change.onclick = function(){
            //     file.click();
            // }

            operate.appendChild(deletes);
            operate.appendChild(change);
            operate.appendChild(ok);

            document.body.appendChild(operate);//水印

            deletes.addEventListener('click',function(){
                file.parentNode.className = 'upload';
                file.parentNode.setAttribute('data-id','');
                var dataid = file.parentNode.getAttribute('data-id');
                imgout();
                file.value = '';
            });
            if(document.getElementById(up)){
                document.getElementById(up).className = 'change';
            }
        }
    },
    fileupload(file){
        if(file.files.length !== 0){
            if(file.files[0].size>5242880){
                var div = document.createElement('div');
                div.className = 'errmsg';
                div.appendChild(document.createTextNode('图片不能超过5M！'));
                document.body.appendChild(div);
                setTimeout(function(){
                    div.parentNode.removeChild(div);
                },3000);

                return;
            }
            file.parentNode.className = 'upload ready';
        }else{
            file.parentNode.className = 'upload';
            return false;
        }

        if(document.getElementsByClassName('showimg').length !== 0){
            var reader = new FileReader();
            reader.readAsDataURL(file.files[0]);
            var a = document.getElementsByClassName('showimg')[0];
            reader.onload = function(e) {
                var data = e.target.result;
                var img = new Image;
                img.src = data;
                img.onload = function(){
                    if(img.width>document.body.scrollWidth){
                        a.style.backgroundSize='contain';
                    }else{
                        a.style.backgroundSize = '';
                    }
                    a.style.backgroundImage = 'url('+data+')';
                }
            };
        }
    }
};