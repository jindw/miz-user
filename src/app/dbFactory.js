// var _ = require('lodash/core');
var ua = navigator.userAgent;
var isMobile = !!ua.match(/mobile/i);
var Q = require('q');
var Util = require('./util');

/**
 * 目前该项目只支持创建一个DB上下文
 * 即所有的DB实例都保存在`DBFactory.context`下
 *
 * TODO 后续增加对复杂项目的支持，允许同时创建多个`DBFactory.context`，比如
 *      一个模块对应一个`DB context`
 *      DBFactory.createContext('Xxx').create('Xxx', methods)
 */
var DBFactory = {
    __: {},
    set: function(key, value) {
        this.__[key] = value;
    },
    get: function(key) {
        return this.__[key];
    },
    create: function(name, methods) {
        // 禁止创建重名的DB实例
        if (this.context[name]) {
            console.warn('DB: "' + name + '" is existed! ');
            return;
        }
        return this.context[name] = new DB(name, methods);
    },
    // 存储db实例
    context: {
        link: function(data) {
            DBFactory.context.Data = data;
        },
        // 占位 禁止覆盖
        Data: {}
    }
};

/**
 *
 */
// 开发环境
if (__LOCAL__) {
    var isGlobalForceMock = true;
} else {
    var isGlobalForceMock = false;
}


function DB(DBName, methods) {
    var t = this;
    t.cache = {};
    $.each(methods, function(method, config) {
        if (typeof config === 'function') {
            t[method] = config;
            return;
        }

        t[method] = function(query) {
            var cfg = {};
            cfg.processData = config.processData; //必须为 Key/Value 格式
            cfg.contentType = config.contentType;

            cfg.method = method;

            cfg.DBName = DBName;

            cfg.query = $.extend({}, config.query || {}, query || {});

            t.urlPrefix = DBFactory.get('urlPrefix') || '';

            // 是否是外部的api
            cfg.isWebApi = typeof config.isWebApi === 'boolean' ? config.isWebApi : false;

            cfg.url = t.getUrl(config.url, cfg.isWebApi);

            cfg.parseResp = config.parseResp || '';

            // 是否是全局只获取一次
            cfg.once = typeof config.once === 'boolean' ? config.once : false;

            // 数据缓存，如果`once`设置为true，则在第二次请求的时候直接返回改缓存数据。
            t.cache[method] = t.cache[method] || null;

            cfg.jsonp = config.jsonp || false;
            cfg.type = config.type || 'POST';
            return request(cfg, t);
        }

        if (config.test) {
            console.log('_____【 ' + DBName + '.' + method + '() 】_____');
            t[method]().then(function(data) {
                console.log(data);
            });
        }

    })
}

$.extend(DB.prototype, {
    /**
     * 获取正式接口的完整`url`
     * 如果通过`DB.set('urlPrefix', 'https://xxx')`设置了全局`url`的前缀，则执行补全
     */
    getUrl: function(url, isWebApi) {
        if (isWebApi) {
            return url;
        } else {
            if (this.urlPrefix) {
                return this.urlPrefix + url;
            } else {
                return url;
            }
        }

    }
});
function request(cfg, db) {
    var deferred = Q.defer();
    if (cfg.once && db.cache[cfg.method]) {
        deferred.resolve(db.cache[cfg.method]);
    } else {
        var query;

        var mergeQuery = {
            os:'H5',
            userKey:localStorage.auth,
        };

        query = $.extend({}, mergeQuery, cfg.query);

        var resfparam = cfg.url.match(/\{\w+\}/g);
        var omit = [];
        if (resfparam && resfparam.length) {
            var temp = '';
            for (var i = resfparam.length - 1; i >= 0; i--) {
                temp = resfparam[i].replace('{', '').replace('}', '');
                cfg.url = cfg.url.replace(resfparam, query[temp] || '');
                omit.push(temp);
            }
        }
        if (cfg.query && (cfg.query.datatype === 'formdata')) {
            query = cfg.query.data;
        }
        
        $.ajax({
            url: cfg.url,
            type: cfg.type,
            dataType: 'json',
            data: query,
            processData: cfg.processData === false ? false : true,
            contentType: cfg.processData
        })
        .done(resp=> {
            var parseRespFunc = cfg.parseResp || DBFactory.get('defaultParsePesp');
            if (!resp.status) {
                // 需要缓存数据的情况
                cfg.once && (db.cache[cfg.method] = data);
                deferred.resolve(resp);
            } else {
                if(resp.status === 21000){
                    localStorage.auth = '';
                    swal({
                        title:'操作失败',
                        text:resp.errorMsg,
                        type:'error',
                        confirmButtonText:'确定',
                        allowOutsideClick: false,
                    }).then(()=>Util.getUserKey()).done();
                }else{
                    deferred.reject(resp);
                }
            }
        })
        .fail(()=> {
            swal({
                title:'服务器连接失败',
                text:'请稍后再试',
                type:'error',
                confirmButtonText:'确定',
                allowOutsideClick: false,
            }).then(()=>history.go(-1)).done();
        });
    }

    return deferred.promise;
}

module.exports = DBFactory;
