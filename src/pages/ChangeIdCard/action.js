var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getMessage',//查看或编辑
    'changeIDCard',//修改
    'addIDCard',//添加
    'idCardInfo',//获取原身份证号信息
]);