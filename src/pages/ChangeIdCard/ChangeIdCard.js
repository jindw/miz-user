/**************************************************\
                ID CARD
\**************************************************/
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var _map = require('lodash/map');

var ChangeIdCard = React.createClass({

	mixins: [Reflux.connect(Store)],

	getInitialState(){
        Action.idCardInfo(this.props.location.query.id);
    },

    componentWillMount(){
        Util.getUserKey();
    },

    componentWillUnmount(){
    	$('.submitImg').remove();
    	$('.swal2-confirm').click();
    },

	componentDidMount(){
		__event__.setHeader.dispatch({
            title: '身份证变更及升位',
            rightBtn: false
        });
	},

	upload(up,title){
		var t = this;
		Util.upload(up,title,t.refs[up],t.state.readonly,'身份证变更及升位');
	},

	fileupload(up){
		Util.fileupload(this.refs[up]);
	},

	Submit(){
		var t = this;
		var newidcard = t.refs.newidcard.value;
		var oldidcard = this.state.idCard;
		var reason = t.refs.reason.value;

		var idcardon = t.refs.idcardon;
		var idcardoff = t.refs.idcardoff;
		var idcardadd = t.refs.idcardadd;

		var patrn=/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;//ID-CARD

		var txt;
		var formData = new FormData();

		if(!patrn.exec(newidcard)){
			txt = '请输入正确的新身份证号';
		}else if(newidcard === oldidcard){
			txt = '新老身份证号相同';
		}

		if(!t.state.change&&!txt){//新增
			if(!idcardon.files.length){
				txt = '请上传新身份证正面照片';
			}else if(!idcardoff.files.length){
				txt = '请上传新身份证反面照片';
			}

			if(!txt){
				formData.append('file', idcardon.files[0]);
				formData.append('file', idcardoff.files[0]);
				formData.append('types', 'NEW_IDCARD_FRONT');
				formData.append('types', 'NEW_IDCARD_BACK');
				if(idcardadd.files.length){
					formData.append('file', idcardadd.files[0]);
					formData.append('types', 'ADDITIONAL_DATA');
				}
			}
		}else if(!txt){//修改
			var filechange = false;

			if(idcardoff.files.length){//有改动
				formData.append('file', idcardoff.files[0]);
				formData.append('types','NEW_IDCARD_BACK');
				filechange = true;
			}else if(!idcardoff.parentNode.getAttribute('data-id')){
				txt = '请上传身份证反面照';
			}

			if(idcardon.files.length){//有改动
				formData.append('file', idcardon.files[0]);
				formData.append('types','NEW_IDCARD_FRONT');
				filechange = true;
			}else if(!idcardon.parentNode.getAttribute('data-id')){//点了删除
				txt = '请上传身份证正面照';
			}

			var chooseimg = false;
			if(t.state.hadadd){//本来有
				if(idcardadd.files.length){//有改动
					formData.append('file', idcardadd.files[0]);
					formData.append('types','ADDITIONAL_DATA');
					filechange = true;
				}else{//删除
					chooseimg = true;
				}
			}else{//本来就没有
				if(idcardadd.files.length){//新增
					formData.append('file', idcardadd.files[0]);
					formData.append('types','ADDITIONAL_DATA');
					filechange = true;
				}
			}
			formData.append('option', chooseimg);
			if(!filechange){
				formData.append('file', null);
				formData.append('types',null);
			}
		}

		if(!txt&&!reason){
			txt = '请输入变更/升位原因';
		}

		if(txt){
			swal({
				title:'温馨提示',
			  	text: txt,
			  	timer: 2000,
			  	type: 'error',
			  	confirmButtonText:'确定',
			}).done();
		}else{
			formData.append('newInfo', newidcard);//sfz
			formData.append('oldInfo', oldidcard);
			formData.append('replaceReason', reason);
			formData.append('userKey', localStorage.auth);
			formData.append('os', 'h5');

			if(t.state.id){//修改
				Action.changeIDCard(formData);
			}else{
				Action.addIDCard(formData);
			}
		}
	},
	componentDidUpdate(){
		var t = this;
		if(t.state.readonly){
			$('input[type=file]').attr('disabled',1);
		}

		t.refs['newidcard'].getDOMNode().value = t.state.record.newInfo||'';
		// t.refs['oldidcard'].getDOMNode().value = t.state.record.oldInfo||'';
		t.refs['reason'].getDOMNode().value = t.state.record.replaceReason||'';

		_map(t.state.pics, function(itm, ind){
			var dt = document.getElementsByClassName('upload');
			if(itm.type === "NEW_IDCARD_FRONT"){//身份证正面照
				dt[0].className = 'upload ready';
				dt[0].setAttribute('data-id',itm.id);
			}else if(itm.type === "NEW_IDCARD_BACK"){//身份证反面照
				dt[1].className = 'upload ready';
				dt[1].setAttribute('data-id',itm.id);
			}else if(itm.type === "ADDITIONAL_DATA"){//补充资料
				dt[2].className = 'upload ready';
				dt[2].setAttribute('data-id',itm.id);
			}
        });
	},

	changeIdcard(idcard){
		var m = this.refs[idcard].getDOMNode();
		m.value = m.value.replace(/\s/g, "");
	},

	blurs(itm){
		if(this.state.readonly){
			this.refs[itm].getDOMNode().blur();
		}
	},

    render() {
        var t = this;
        return (
            <section className = 'changeidcard'>
            	<label>请您按以下顺序提交审核资料</label>

            	<ul className='cardnumber'>
            		<li>
						<div>老身份证号
							<label>{t.state.protectedIdCard}</label>
						</div>
					</li>
					<li>
						<div>新身份证号
							<input onFocus={t.blurs.bind(this,'newidcard')} onBlur={t.changeIdcard.bind(this,'newidcard')}
							ref='newidcard' maxLength = '18' type='text' placeholder='请输入新身份证号' />
						</div>
					</li>
            	</ul>
            	<ul>
					<li>
						<div>新身份证正面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'idcardon','身份证正面照')}>
							<i></i>
							<input ref='idcardon' onChange={t.fileupload.bind(this,'idcardon')}
							type='file' id='idcardon' accept='image/*' />
						</div>
					</li>
					<li>
						<div>新身份证反面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'idcardoff','身份证反面照')}>
							<i></i>
							<input ref='idcardoff' onChange={t.fileupload.bind(this,'idcardoff')}
							id='idcardoff' accept="image/*" type='file' />
						</div>
					</li>
					<li>
						<div>补充资料上传</div>
						<div className='upload' onClick={t.upload.bind(this,'idcardadd','补充资料')}>
							<i></i>
							<input ref='idcardadd' onChange={t.fileupload.bind(this,'idcardadd')}
							type='file' id='idcardadd' accept="image/*"/>
						</div>
					</li>
            	</ul>
            	<span>注：此项为非必选项</span>
            	
            	<ul className='reason'>
					<li>
						<div>
							变更/升位原因
						</div>
					</li>
					<li>
						<textarea ref='reason' placeholder='请输入更换身份证的原因，不超过50字。'
						maxLength="50" onFocus={t.blurs.bind(this,'reason')}></textarea>
					</li>
            	</ul>
            	<a className={t.state.submitdisplay} href='javascript:;' onClick={t.Submit} hidden>提交</a>
            	<input type='hidden' id='J_Title' value='身份证变更及升位'/>
            </section>
        );
    }
});

module.exports = ChangeIdCard;