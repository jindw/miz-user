var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var _assign = require('lodash/assign');
var _map = require('lodash/map');

var s;
var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    idCardInfo(id){
        var t = this;
        DB.IDCard.idCardInfo().then(data=>{
            _assign(t.data,data);
            t.trigger(t.data);
        }, data=>{
            swal({
                title: '温馨提示',
                text: data.errorMsg,
                type: 'error',
                confirmButtonText:'确定',
                timer:2000,
            }).done().then(()=>{
                history.go(-1);
            });
        });
        if(id){
            t.getMessage(id);
        }else{
            t.data.submitdisplay = 'submit';
            t.trigger(t.data);
        }
    },

    getMessage(id){
    	var t = this;

    	DB.Program.getMessage({
    		id:id,
        }).then(data=>{
        	t.data.pics = data.pics;
            if(!!data.record){
                t.data.record = data.record;
                if(data.record.status !== 'CHECK_FAILED'){
                    t.data.readonly = true;
                }else{
                    t.data.submitdisplay = 'submit';
                    t.data.change = true;
                    t.data.id = id;
                }
            }else{
                t.data.readonly = true;
            }

            _map(data.pics,(itm, ind)=>{
                if(itm.type === "ADDITIONAL_DATA"){//补充资料
                    t.data.hadadd = true;
                }
            });
            t.trigger(t.data);
        },data=>{
            t.errmsg(data.errorMsg);
        });
    },

    submitImg(){
        var a = document.createElement('a');
        a.className = 'submitImg';
        document.body.appendChild(a);
    },

    submitImgOver(){
        var submitImg = document.getElementsByClassName('submitImg')[0];
        submitImg.parentNode.removeChild(submitImg);
    },

    addIDCard(data){
        var t = this;
        t.submitImg();
        DB.IDCard.addIDCard({
            datatype:'formdata',
            data:data,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },data=>{
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        });
    },

    changeIDCard(data){
        var t = this;
        t.submitImg();
        DB.IDCard.changeIDCard({
            datatype:'formdata',
            data:data,
            id:t.data.id,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },data=>{
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        })
    },

    errmsg(err){
        swal({
            title:'温馨提示',
            text: err,
            timer: 2000,
            type: 'error',
            confirmButtonText:'确定',
        }).done();
    },

    getInitialState() {
        var t = this;
        this.data = {
            record:[],
            readonly:false,
            pics:[],
            submitdisplay:'',
            change:false,
            id:0,
            hadadd:false
        };
        return this.data;
    }
});

module.exports = Store;