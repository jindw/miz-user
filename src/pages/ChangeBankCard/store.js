var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var _map = require('lodash/map');
var _find = require('lodash/find');
var _includes = require('lodash/includes');

var s;
var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    bankInfo(id){
        var data,t=this;
        if(!id){
            DB.BankCard.bankInfo().then(data=>next(data), data=>error(data.errorMsg)); 
        }else{
            DB.Program.getMessage({id:id}).then(data=>{
                next(data);
                this.getMessage(data);
            }, data=>error(data.errorMsg)); 
        }

        function next(data) {
            var banks = []
            _map(data.bankInfo,(itm,ind)=>{
                if(_find(banks,{id:itm.bankName}))return;
                banks.push({
                    id:itm.bankName,
                    text:itm.bankName,
                });
            });
           
            $('.mybank').select2({
                data: banks,
                theme: "classic",
            }).on('change', e => {
                if(_includes(e.currentTarget.className,'oldbank')){
                    mycardnumber();
                }else{
                    newcardnumber();
                }
            }).trigger('change');

            function mycardnumber() {
                var bankName = $('.oldbank+span .select2-selection__rendered').text();
                var bankcards = [];
                _map(data.bankInfo,(itm,ind)=>{
                    var cardNumber = itm.cardNumber;
                    if(itm.bankName === bankName){
                        bankcards.push({
                            id:itm.cardNumber,
                            text:`${cardNumber.substring(0,4)}********${cardNumber.substring(cardNumber.length-4)}`,
                        });
                    }
                });
                var onum = $('.oldbankNum');
                onum.html('').select2({
                    data: bankcards,
                    theme: "classic",
                });
            }

            function newcardnumber() {
                var bankName = $('.newbank+span .select2-selection__rendered').text();
                var bankcards = [];
                _map(data.bankInfo,(itm,ind)=>{
                    var cardNumber = itm.cardNumber;
                    if(itm.bankName === bankName){
                        bankcards.push({
                            id:itm.cardNumber,
                            text:`${cardNumber.substring(0,4)}********${cardNumber.substring(cardNumber.length-4)}`,
                        });
                    }
                });
                var onum = $('.newbankNum');
                onum.html('').select2({
                    data: bankcards,
                    theme: "classic",
                });
            }
            if(!id){
                t.data.submitdisplay = 'submit';
                t.trigger(t.data);
            }
        }

        function error(err) {
            swal({
                title: '温馨提示',
                text:err,
                type: 'error',
                confirmButtonText:'确定',
                timer:2000,
            }).done().then(()=>{
                history.go(-1);
            });
        }

        
    },

    getMessage(data){
        var t = this;
        t.data.pics = data.pics;
        if(data.record){
            t.data.record = data.record;
            var reasons = $('#reasons');
            if(!_includes(["原卡注销","原卡不再使用","原卡限额低"],data.record.replaceReason)){
                $('#otherReason').val(data.record.replaceReason);
                reasons.val('null');
            }else{
                reasons.val(data.record.replaceReason);
            }

            $('.newbank').val(data.record.newBank);
            $('.oldbank').val(data.record.oldBank);
            $('#reasons,.mybank').trigger('change');
            $('.newbankNum').val(data.record.newInfo);
            $('.oldbankNum').val(data.record.oldInfo);
            $('.bankCardNum').trigger('change');
            // if(!$('.newbank').val()){
            //     $('.newbank').val($('.newbank option').val());
            //     $('.oldbank').val($('.oldbank option').val());
            //     $('.mybank').trigger('change');
            // }

            if(data.record.status !=='CHECK_FAILED'){
                t.data.readonly = true;
                $('.mybank,.bankCardNum,#reasons').prop("disabled", true);
            }else{
                t.data.submitdisplay = 'submit';
                t.data.change = true;
            }
        }else{
            t.data.readonly = true;
        }
        t.trigger(t.data);
    },

    submitImg(){
        var a = document.createElement('a');
        a.className = 'submitImg';
        document.body.appendChild(a);
    },

    submitImgOver(){
        var submitImg = document.getElementsByClassName('submitImg')[0];
        submitImg.parentNode.removeChild(submitImg);
    },

    addBankCard(data){
        var t = this;
        t.submitImg();
        DB.BankCard.addBankCard({
            datatype:'formdata',
            data:data,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },function(data){
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        })
    },

    changeBankCard(data,id){
        var t = this;
        t.submitImg();
        DB.BankCard.changeBankCard({
            datatype:'formdata',
            data:data,
            id:id,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },data=>{
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        });
    },

    errmsg(err){
        swal({
            title: '温馨提示',
            text: err,
            type: 'error',
            confirmButtonText:'确定',
            timer: 2000,
        }).done();
    },

    getInitialState() {
        var t = this;
        this.data = {
            record:{replaceReason:''},
            readonly:false,
            pics:[],
            submitdisplay:'',
            change:false
        };
        return this.data;
    }
});

module.exports = Store;