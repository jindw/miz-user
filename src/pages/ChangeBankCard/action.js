var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getMessage',//查看或编辑
    'changeBankCard',//update
    'addBankCard',//add
    'bankInfo',//获取新老银行及银行卡信息
]);