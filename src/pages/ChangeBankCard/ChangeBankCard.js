/**************************************************\
                BANK CARD
\**************************************************/
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var _map = require('lodash/map');
var _includes = require('lodash/includes');

var ChangeIdCard = React.createClass({

	mixins: [Reflux.connect(Store)],

	getInitialState(){
        Action.bankInfo(this.props.location.query.id);
        return {
        	others:false,
        }
    },

    componentWillMount(){
        Util.getUserKey();
    },

    componentWillUnmount(){
    	$('.submitImg').remove();
    	$('.swal2-confirm').click();
    },

	componentDidMount(){
		var t = this;
		__event__.setHeader.dispatch({
            title: '更换银行卡',
            rightBtn: 'bank-reason'
        });
        $('.select2').select2({
        	placeholder:'loading...'
        });
        $(this.refs.selectReason).select2({theme: "classic"}).on('change', ()=> {
        	var reason = $('.reason select').val();
        	t.setState({
        		others: reason === 'null',
        		reasons:reason,
        	});
        }).trigger('change');
	},

	upload(up,title){
		Util.upload(up,title,this.refs[up],this.state.readonly,'更换银行卡');
	},

	fileupload(up){
		Util.fileupload(this.refs[up]);
	},

	Submit(){
		var t = this;
		var newbankcard = $('.newbankNum option:selected').val();
		var oldbankcard = $('.oldbankNum option:selected').val();
		var newbankname = $('.newbank+span .select2-selection__rendered').text();
		var oldbankname = $('.oldbank+span .select2-selection__rendered').text();
		var reason = this.state.others?this.refs.reason.value:this.state.reasons;

		//images
		var bankidcardon = t.refs.bankidcardon;
		var bankidcardoff = t.refs.bankidcardoff;
		var bankbankon = t.refs.bankbankon;
		var bankbankoff = t.refs.bankbankoff;

		var patrn= /^\d+$/;

		var txt;
		var formData = new FormData();

		if(!newbankname){
			txt = '请选择新银行名称';
		}else if(!newbankcard){
			txt = '请选择新银行卡号';
		}else if(!oldbankname){
			txt = '请选择老银行名称';
		}else if(!oldbankcard){
			txt = '请选择老银行卡号';
		}else if(!reason.length){
			txt = '请选择或输入更换银行卡的原因';
		}

		if(!txt&&!t.state.change){//新增
			if(!bankidcardon.files.length){
				txt = '请上传双手手持身份证正面照片';
			}else if(!bankidcardoff.files.length){
				txt = '请上传双手手持身份证反面照片';
			}else if(!bankbankon.files.length){
				txt = '请上传双手手持新银行卡正面照片';
			}else if(!bankbankoff.files.length){
				txt = '请上传双手手持新银行卡反面照片';
			}

			if(!txt){
				formData.append('file', bankidcardon.files[0]);
				formData.append('file', bankidcardoff.files[0]);
				formData.append('file', bankbankon.files[0]);
				formData.append('file', bankbankoff.files[0]);
				formData.append('types', ['HANDHELD_IDCARD_FRONT','HANDHELD_IDCARD_BACK','HANDHELD_BANKCARD_FRONT','HANDHELD_BANKCARD_BACK']);
			}
		}else if(!txt){//UPDATE
			var filechange = false;
			if(bankidcardon.files.length){//有改动
				formData.append('file', bankidcardon.files[0]);
				formData.append('types','HANDHELD_IDCARD_FRONT');
				filechange = true;
			}else if(!bankidcardon.parentNode.getAttribute('data-id')){//点了删除
				txt = '请上传双手手持身份证正面照片';
			}

			if(bankidcardoff.files.length){//有改动
				formData.append('file', bankidcardoff.files[0]);
				formData.append('types','HANDHELD_IDCARD_BACK');
				filechange = true;
			}else if(!bankidcardoff.parentNode.getAttribute('data-id')){//点了删除
				txt = '请上传双手手持身份证反面照片';
			}

			if(bankbankon.files.length){//有改动
				formData.append('file', bankbankon.files[0]);
				formData.append('types','HANDHELD_BANKCARD_FRONT');
				filechange = true;
			}else if(!bankbankon.parentNode.getAttribute('data-id')){//点了删除
				txt = '请上传双手手持新银行卡正面照片';
			}

			if(bankbankoff.files.length){//有改动
				formData.append('file', bankbankoff.files[0]);
				formData.append('types','HANDHELD_BANKCARD_BACK');
				filechange = true;
			}else if(!bankbankoff.parentNode.getAttribute('data-id')){//点了删除
				txt = '请上传双手手持新银行卡反面照片';
			}

			if(!filechange){
				formData.append('file', null);
				formData.append('types',null);
			}
		}

		if(txt){
			swal({
				title:'温馨提示',
			  	text: txt,
			  	timer: 2000,
			  	type: 'error',
			  	confirmButtonText:'确定',
			}).done();
		}else{
			formData.append('replaceReason', reason);
			formData.append('userKey', localStorage.auth);
			formData.append('os', 'h5');
			formData.append('newBank', newbankname);
			formData.append('oldBank', oldbankname);
			formData.append('newInfo', newbankcard);
			formData.append('oldInfo', oldbankcard);
			if(t.props.location.query.id){//修改
				Action.changeBankCard(formData,t.props.location.query.id);
			}else{
				Action.addBankCard(formData);
			}
		}
	},

	componentDidUpdate(){
		var t = this;
		if(t.state.readonly){
			$('input[type=file]').attr('disabled',1);
		}

		// t.refs['newbankname'].value = t.state.record.newBank||'';

		// t.refs['reason'].value = t.state.record.replaceReason||'';

		// t.refs['newbankcard'].value = t.state.record.newInfo||'';

		// t.refs['oldbankname'].value = $('#oldbank+span .select2-selection__rendered').text();
		// t.refs['oldbankcard'].value = $('#oldbankNum+span .select2-selection__rendered').text();

		_map(t.state.pics, function(itm, ind){
			var dt = document.getElementsByClassName('upload');
			if(itm.type === "HANDHELD_BANKCARD_FRONT"){
				dt[2].className = 'upload ready';
				dt[2].setAttribute('data-id',itm.id);
			}else if(itm.type === "HANDHELD_IDCARD_BACK"){
				dt[1].className = 'upload ready';
				dt[1].setAttribute('data-id',itm.id);
			}else if(itm.type === "HANDHELD_BANKCARD_BACK"){
				dt[3].className = 'upload ready';
				dt[3].setAttribute('data-id',itm.id);
			}else if(itm.type === "HANDHELD_IDCARD_FRONT"){
				dt[0].className = 'upload ready';
				dt[0].setAttribute('data-id',itm.id);
			}
        });
	},

	blurs(itm){
		if(this.state.readonly){
			this.refs[itm].blur();
		}
	},

    render() {
        var t = this;

        return (
            <section className = 'changeidcard bank'>
            	<label>请您按以下顺序提交审核资料</label>
            	<ul className='cardnumber'>
					<li>
						<div>老银行名称
							<select className='mybank oldbank select2'></select>
						</div>
					</li>
					<li>
						<div>老银行卡号
							<select className='oldbankNum select2 bankCardNum'></select>
						</div>
					</li>
				</ul>
				<ul className='cardnumber'>
					<li>
						<div>新银行名称
							<select className='mybank newbank select2'/>
						</div>
					</li>
					<li>
						<div>新银行卡号
							<select className='newbankNum select2 bankCardNum'/>
						</div>
					</li>
            	</ul>
            	<span className='bind'>注：提交换卡申请前，新卡必须是已绑定状态</span>

            	<ul>
					<li>
						<div>双手手持身份证正面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'bankidcardon','手持身份证正面照')}>
							<i></i>
							<input onChange={t.fileupload.bind(this,'bankidcardon')} type='file' ref = 'bankidcardon' accept="image/*" />
						</div>
					</li>
					<li>
						<div>双手手持身份证反面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'bankidcardoff','手持身份证反面照')}>
							<i></i>
							<input onChange={t.fileupload.bind(this,'bankidcardoff')} type='file' ref = 'bankidcardoff' accept="image/*" />
						</div>
					</li>
					<li>
						<div>双手手持新银行卡正面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'bankbankon','手持银行卡正面照')}>
							<i></i>
							<input onChange={t.fileupload.bind(this,'bankbankon')} type='file' ref = 'bankbankon' accept="image/*" />
						</div>
					</li>
					<li>
						<div>双手手持新银行卡反面照片</div>
						<div className='upload' onClick={t.upload.bind(this,'bankbankoff','手持银行卡反面照')}>
							<i></i>
							<input onChange={t.fileupload.bind(this,'bankbankoff')} type='file'
							ref = 'bankbankoff' accept="image/*" />
						</div>
					</li>
            	</ul>
				
            	<ul className='reason'>
					<li>
						<div>更换银行卡号原因
						</div>
					</li>
					<li>
						<select ref='selectReason' id='reasons'>
						  	<option value="原卡注销">原卡注销</option>
						  	<option value="原卡不再使用" selected={this.state.record.replaceReason === "原卡不再使用"}>原卡不再使用</option>
						  	<option value="原卡限额低" 
						  	selected={this.state.record.replaceReason === "原卡限额低"}>原卡限额低</option>
						  	<option value="null">其他</option>
						</select>
						<textarea style={{display:(this.state.others?'':'none')}}
						ref='reason' placeholder='请输入更换银行卡号的原因，不超过50字。'
						maxlength="50" onFocus={t.blurs.bind(this,'reason')} 
						id='otherReason'
						/>
					</li>
            	</ul>
            	<a hidden onClick={t.Submit} href='javascript:;' className={t.state.submitdisplay}>提交</a>
            	<input type='hidden' id='J_Title' value='更换银行卡'/>
            </section>
        );
    }
});

module.exports = ChangeIdCard;