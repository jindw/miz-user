var Util = require('../../app/util');
var UserMsgChange = React.createClass({
	componentDidMount(){
		__event__.setHeader.dispatch({
            title: '用户信息更换',
            backBtn: true,
            rightBtn: 'caips'
        });
        scroll(0,0);
	},

    send(toWhere){
        if(!localStorage.auth)Util.getUserKey();
        location.hash = `#/${toWhere}`;
    },

    render() {
        var t = this;
        return (
                <section className = 'usermsgchange'>
                	<ul>
                		<li onClick={t.send.bind(null,'changeidcard')}>
                            <i></i><div>身份证变更及升位</div>
                        </li>
                        <li onClick={t.send.bind(null,'changebankcard')}>
                            <i className='icon-bank'></i><div>更换银行卡</div>
                        </li>
                        <li onClick={t.send.bind(null,'changemobile')}>
                            <i className='icon-phone'></i><div>更换手机号码</div>
                        </li>
                	</ul>
                    <a id='submit' href='#/caips'>进度查询</a>
                </section>
        );
    }
});

module.exports = UserMsgChange;