var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getMessage',//查看或编辑
    'addMobile',//新增
    'changeMobile',//修改
    'getUserInfo',//获取信息
    'sendVerificationCode',//发送验证码
    'validate',//校验新手机号码
    'replaceMobile',//提交
    'checkRegisterable',//验证是否注册过
    'closemodalPsd'//关闭输入密码窗口
]);