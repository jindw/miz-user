var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var _assign = require('lodash/assign');
var s;
var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    checkRegisterable(mobile,imgCode){
        var t = this;
        DB.Mobile.checkRegisterable({
            mobile:mobile
        }).then(data=>{
            t.sendVerificationCode(mobile,imgCode);
        }, data=>{
            swal({
                title: '操作失败',
                text: data.errorMsg,
                type: 'error',
                timer:2000,
                confirmButtonText:'确定',
            }).done();
        });
    },

    replaceMobile(message){
        var t = this;
        DB.Mobile.replaceMobile(message).then(data=> {
            swal({
                title:'操作成功',
                text:'已成功更换手机号码',
                type:'success',
                confirmButtonText:'确定',
                allowOutsideClick: false,
            }).then(()=>{
                location.hash = '#/';
            }).done();
        }, data=> {
            swal({
                title: '操作失败',
                text: data.errorMsg,
                type: 'error',
                confirmButtonText:'确定',
            }).done();
        })
    },

    validate(message){
        var t = this;
        DB.Mobile.validate(message).then(data=> {
            t.data.psdOpen = true;
            t.trigger(t.data);
        }, data=> {
            swal({
              title: '操作失败',
              text:data.errorMsg,
              type:'error',
              timer:2000,
              confirmButtonText:'确定',
            }).done();
        })
    },

    afterSendCode(){
        var time = 60;
        this.data.couldSend = false;
        var tt = setInterval(()=>{
            this.data.mailCode = `(${time--}s)后再次发送`;
            if(time <= 1){
                clearInterval(tt);
                this.data.couldSend = true;
                this.data.mailCode = '重发校验码';
            }
            $(window).one('hashchange',()=>clearInterval(tt));
            this.trigger(this.data);
        },1000);
    },

    sendVerificationCode(mobile,checkCode){
        var t = this;
        DB.Mobile.sendVerificationCode({
            mobile:mobile,
            checkCode:checkCode,
        }).then(data=> {
            t.afterSendCode();
        }, data=> {
            swal({
                title: '操作失败',
                text:data.errorMsg,
                timer: 2000,
                type: 'error',
                confirmButtonText:'确定',
            }).done();
        })
    },

    getUserInfo(id){
        var t = this;
        DB.Mobile.getUserInfo().then(data=> {
            _assign(t.data,data);
            t.trigger(t.data);
        }, data=> {
            swal({
                title: '操作失败',
                text: data.errorMsg,
                timer: 2000,
                type: 'error',
                confirmButtonText:'确定',
            }).done().then(()=>history.go(-1));
        });

        if(!id){
            t.data.submitdisplay = 'submit';
            t.trigger(t.data);
        }else{
            this.getMessage(id);
        }
    },

    getMessage(id){
    	var t = this;
    	DB.Program.getMessage({
    		id:id,
        }).then(data=>{
        	t.data.pics = data.pics;
        	if(!!data.record){
        		t.data.record = data.record;
        		if(data.record.status !=='CHECK_FAILED'){
	        		t.data.readonly = true;
	        	}else{
	        		t.data.submitdisplay = 'submit';
	        		t.data.change = true;
	        		t.data.id = id;
	        	}
        	}else{
        		t.data.readonly = true;
        	}

            t.trigger(t.data);
        },data=>t.errmsg(data.errorMsg));
    },
    submitImg(){
        var a = document.createElement('a');
        a.className = 'submitImg';
        document.body.appendChild(a);
    },
    submitImgOver(){
        var submitImg = document.getElementsByClassName('submitImg')[0];
        submitImg.parentNode.removeChild(submitImg);
    },

    addMobile(data){
        var t = this;
        t.submitImg();
        DB.Mobile.addMobile({
            datatype:'formdata',
            data:data,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },data=>{
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        })
    },

    changeMobile(data){
        var t = this;
        t.submitImg();
        DB.Mobile.changeMobile({
            datatype:'formdata',
            data:data,
            id:t.data.id,
        }).then(data=>{
            t.submitImgOver();
            location.hash = 'caips';
        },data=>{
            t.submitImgOver();
            t.errmsg(data.errorMsg);
        })
    },

    closemodalPsd(){
        this.data.psdOpen = false;
        this.trigger(this.data);
    },

    errmsg(err){
        swal({
            title:'温馨提示',
            text: err,
            timer: 2000,
            type: 'error',
            confirmButtonText:'确定',
        }).done();
    },

    getInitialState: function() {
        var t = this;
        this.data = {
            record:[],
            readonly:false,
            pics:[],
            submitdisplay:'',
            change:false,
            id:0,
            user:[],
            mailCode:'获取校验码',
            couldSend:true,
            psdOpen:false,
        };
        return this.data;
    }
});

module.exports = Store;