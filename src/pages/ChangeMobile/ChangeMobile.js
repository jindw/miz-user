/**************************************************\
                MOBILE
\**************************************************/

var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var md5 = require('md5');

var ChangeMobile = React.createClass({

	mixins: [Reflux.connect(Store)],

	getInitialState(){
        Action.getUserInfo(this.props.location.query.id);
        var imgCodeTitle = '//api.mizlicai.com';
		if (__LOCAL__) {
			imgCodeTitle = '//121.43.148.191:8108';
		}
        return {
        	imgCodeTitle:imgCodeTitle,
        	imgCode:`${imgCodeTitle}/randcode/randCode.json?time=${Date.now()}`
        }
    },

    componentWillUnmount(){
    	$('.submitImg').remove();
    	$('.swal2-confirm').click();
    },

	componentDidMount(){
		__event__.setHeader.dispatch({
            title: '更换手机号码',
            rightBtn: false
        });
	},

	upload(up,title){
		Util.upload(up,title,this.refs[up],this.state.readonly,'更换手机号码');
	},

	fileupload(up){
		Util.fileupload(this.refs[up]);
	},

	Submit(){
		var newphone = this.refs.newphone.value;
		var imgCode = this.refs.imgCode.value;
		var code = this.refs.code.value;

		var err;
		if(!newphone){
			err = '请输入更换的手机号';
		}else if(!imgCode){
			err = '请输入正确的图形验证码';
		}else if(!code){
			err = '请输入正确的短信验证码';
		}
		if(err){
			swal({
				title: '操作失败',
			  	text: err,
			  	timer: 2000,
			  	type: 'warning',
			  	confirmButtonText:'确定',
			}).done();
		}else{
			Action.validate({
				mobile:newphone,
				code:code,
			});
		}	
	},

	closePsd(){
		Action.closemodalPsd();
	},

	changeMobile(mobile){
		var m = this.refs[mobile].getDOMNode();
		m.value = m.value.replace(/\D/g, "")
	},

	blurs(itm){
		if(this.state.readonly){
			this.refs[itm].getDOMNode().blur();
		}
	},

	changeImgCode(){
		var t = this;
		this.setState({
			imgCode:`${t.state.imgCodeTitle}/randcode/randCode.json?time=${Date.now()}`
		});
	},

	sendMessage(){
		if(!this.state.couldSend)return;
		var newphone = this.refs.newphone.value;
		var imgCode = this.refs.imgCode.value;

		var err;
		if(!newphone){
			err = '请输入更换的手机号';
		}else if(!imgCode){
			err = '请输入正确的图形验证码';
		}
		if(err){
			swal({
				title: '操作失败',
			  	text: err,
			  	timer: 2000,
			  	type: 'warning',
			  	confirmButtonText:'确定',
			}).done();
		}else{
			Action.checkRegisterable(newphone,imgCode);
		}
	},

	sureSubmit(){
		var loginPassword = this.refs.loginPassword.value;
		if(!loginPassword){
			swal({
				title:'温馨提示',
			  	text: '请输入正确的登录密码',
			  	timer: 2000,
			  	confirmButtonText:'确定',
			  	type:'error',
			}).done();
		}else{
			Action.replaceMobile({
				code:this.refs.code.value,
				mobile:this.refs.newphone.value,
				password:md5(loginPassword),
			});
		}
	},

	forgetPsd(){
		location.href = `${location.origin}/#/resetPassword`
	},

    render() {
        var t = this;
        return <section className = 'changeidcard changeMobile'>
            	<label>更换手机号码</label>
            	<ul className='cardnumber top'>
					<li>
						<div>账户
							<label>{t.state.user.protectedMobile}</label>
						</div>
					</li>
					<li>
						<div>姓名
							<label>{t.state.user.protectedName}</label>
						</div>
					</li>
					<li>
						<div>身份证
							<label>{t.state.user.protectedIdCard}</label>
						</div>
					</li>
            	</ul>

            	<ul className='cardnumber bottom'>
					<li>
						<div>新手机号
							<input onFocus={t.blurs.bind(this,'newphone')} 
							onChange={t.changeMobile.bind(this,'newphone')}
							ref='newphone' name='newphone' type='tel' 
							placeholder='请输入更换手机号' maxLength='11'/>
						</div>
					</li>
					<li className='imgCode'>
						<div>图形验证码
							<input onFocus={t.blurs.bind(this,'imgCode')} 
							ref='imgCode' name='imgCode' type='text' 
							placeholder='图形验证码' maxLength='4'/>
							<img onClick={this.changeImgCode} src={this.state.imgCode} alt=""/>
						</div>
					</li>
					<li>
						<div>短信验证码
							<input onFocus={t.blurs.bind(this,'code')} 
							onChange={t.changeMobile.bind(this,'code')}
							ref='code' name='code' type='tel' 
							placeholder='短信验证码' maxLength='6'/>
							<a href="javascript:;" onClick={this.sendMessage}>{this.state.mailCode}</a>
						</div>
					</li>
            	</ul>
            	<a style={{display:(t.state.submitdisplay?'':'none')}} 
            	href='javascript:;' onClick={t.Submit} 
            	className={t.state.submitdisplay}>确认</a>
            	<div style={{display:(this.state.psdOpen?'':'none')}} className='loginPsd'>
            		<a href="javascript:;"></a>
            		<div>
            			<p onClick={this.closePsd}>登录密码校验<a>&times;</a></p>
            			<div>
							<input ref='loginPassword' type="password" autoFocus/>
							<a href="javascript:;" onClick={this.forgetPsd}>忘记登录密码?</a>
            			</div>
            			<a href="javascript:;" onClick={t.sureSubmit}>确定</a>
            		</div>
            	</div>
            </section>;
    }
});

module.exports = ChangeMobile;