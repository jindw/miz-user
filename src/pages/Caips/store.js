var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');

var s;
var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    getProgress:function(productId){
        var t = this;
        DB.Progress.getList().then(function(data){
        	t.data.list = data.list;
            t.data.status = data.status;
            if(data.list.length){
                t.data.caipsh = 'auto';
            }
            t.trigger(this.data);
        });
    },

    getInitialState: function() {
        var t = this;
        this.data = {
            list: [],
            status:'',
            caipsh:document.body.scrollHeight-45
        };
        return this.data;
    }
});

module.exports = Store;