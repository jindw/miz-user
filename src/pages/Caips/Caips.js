/**************************************************\
                进度查询
\**************************************************/

var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var _map = require('lodash/map');


var ChangeIdCard = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState(){
        Action.getProgress();
    },

    componentWillMount(){
        Util.getUserKey();
    },

	componentDidMount(){
		__event__.setHeader.dispatch({
            title: '进度查询',
            rightBtn:'tel',
            backBtn:'home'
        });
	},
    componentDidUpdate(){
        this.reason();
    },

	reason(){
		var reasons = document.querySelectorAll('.reason label');
		var tip,more,t=this;
		for (var i = 0,sum = reasons.length; i<sum; i++) {
			if(reasons[i].scrollHeight>50){
				reasons[i].parentNode.className = 'reason more';
                more();
            }
		}
        function more(){
            $('body').off('click').on('click', '.more a', function() {
                var that = $(this);
                that.prev().css({
                    height: 42,
                    maxHeight: 'initial'
                });
                that.prev().animate({
                    height: that.prev()[0].scrollHeight
                },'fast', ()=> {
                    that.text('收起');
                    less();
                });
            });
        }

        function less(){
            $('body').off('click').on('click', '.more a', function() {
                var that = $(this);
                that.prev().animate({
                    height: 42
                },'fast', function() {
                    that.text('更多');
                    more();
                });
            });
        }
	},

    render() {
        var t = this;
        var mylist = t.state.list;
        var tlist = [];
        _map(mylist, function(itm, ind){
            if(ind >= 0){
                var type,time,urls;
                if(itm.type === 'MOBILE'){
                    type = '更换手机号码';
                    urls = 'changemobile';
                }else if(itm.type === 'BANKCARD'){
                    type='更换银行卡';
                    urls = 'changebankcard'
                }else if(itm.type === 'IDCARD'){
                    type='身份证变更及升位';
                    urls = 'changeidcard';
                }

                var date = new Date(itm.modifyDate);
                time = date.getFullYear() + '/'+ (date.getMonth()+1)+'/'+date.getDate()+' '+(date.getHours()>9?date.getHours():'0'+date.getHours())+':'+(date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes());
                tlist.push(
                    <section className={itm.status}>
                        <div className = 'sec'>
                            <ul>
                                <li>
                                    <span>更换项目</span>
                                    <span>{type||'-'}</span>
                                </li>
                                <li>
                                    <span>提交审核时间</span>
                                    <span>{time||'-'}</span>
                                </li>
                                <li>
                                    <span>审核状态</span>
                                    <span>{itm.statusValue}</span>
                                </li>
                            </ul>
                            <a href={'#'+urls+'?id='+itm.id}>{itm.status === 'CHECK_FAILED'?'编辑':'查看'}</a>
                        </div>
                        <div className='reason'>
                            <div>拒绝原因</div>
                            <label>{itm.refuseReasonValue||'-'}{itm.remark?':'+itm.remark:''}</label>
                            <a href='javascript:;'>更多</a>
                        </div>
                    </section>
                );
            }
        });
        if(!tlist.length){
            if(!t.state.status){
                tlist = <section className="loading nothing"></section>;
            }else{
                tlist = <section className="loading"></section>;
            }
        }

        return (
            <section className = 'caips' style={{height:t.state.caipsh}}>
                {tlist}
            </section>
        );
    }
});

module.exports = ChangeIdCard;